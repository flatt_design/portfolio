from django.conf.urls import url

from . import views

urlpatterns = [
	url(r'^$', views.full_portfolio, name='index'),
	url(r'(?P<pk>\d+)/$', views.project_detail, name='detail'),
]