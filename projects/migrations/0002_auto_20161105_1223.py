# -*- coding: utf-8 -*-
# Generated by Django 1.10.2 on 2016-11-05 17:23
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('projects', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='project',
            name='published_at',
            field=models.DateTimeField(blank=True, db_index=True, editable=False, null=True),
        ),
    ]
