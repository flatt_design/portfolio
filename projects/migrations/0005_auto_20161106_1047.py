# -*- coding: utf-8 -*-
# Generated by Django 1.10.2 on 2016-11-06 16:47
from __future__ import unicode_literals

from django.db import migrations
import tinymce.models


class Migration(migrations.Migration):

    dependencies = [
        ('projects', '0004_auto_20161105_1511'),
    ]

    operations = [
        migrations.AlterField(
            model_name='project',
            name='description',
            field=tinymce.models.HTMLField(blank=True),
        ),
    ]
