from django.shortcuts import get_object_or_404, render

from .models import Project, ProjectType

def full_portfolio(request):
	"""
	All available portfolio projects
	"""
	projects = Project.objects.published()
	project_types = ProjectType.objects.all()

	return render(request, 'projects/full_portfolio.html', {
		'projects': projects,
		'project_types': project_types,
	})

def project_detail(request, pk):
	"""
	View a single portfolio project
	"""
	project = get_object_or_404(Project, pk=pk)
	return render(request, 'projects/project_detail.html', {
		'project': project,
	})