from django.contrib import admin

from .models import Project, ProjectType, ProjectImage

class ImageInline(admin.StackedInline):
	model = ProjectImage

class ProjectAdmin(admin.ModelAdmin):
	list_display = ["__str__", "published"]

	def published(self, obj):
		if obj.is_published:
			return True
		else:
			return False
	
	inlines = [ImageInline,]

admin.site.register(Project, ProjectAdmin)
admin.site.register(ProjectType)