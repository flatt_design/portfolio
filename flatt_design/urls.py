"""flatt_design URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from django.contrib import admin
from django.views.static import serve

from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from . import views, settings

urlpatterns = [
    url(r'^posts/', include('posts.urls', namespace='posts')),
	url(r'^portfolio/', include('projects.urls', namespace='projects')),
    url(r'^services/$', views.services, name='services'),
    url(r'^team/$', views.team, name='team'),
    url(r'^contact/$', views.contact, name='contact'),
    url(r'^markdownx/', include('markdownx.urls')),
    url(r'^tinymce/', include('tinymce.urls')),
    url(r'^jet/', include('jet.urls', 'jet')),
    url(r'^jet/dashboard/', include('jet.dashboard.urls', 'jet-dashboard')),
    url(r'^admin/', admin.site.urls),
    url(r'^$', views.index, name='home'),
]

if settings.DEBUG:
    urlpatterns += url(r'^media/(?P<path>.*)$', serve, {
                        'document_root': settings.MEDIA_ROOT,
                    }),

urlpatterns += staticfiles_urlpatterns()
