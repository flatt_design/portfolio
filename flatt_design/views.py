from django.template import RequestContext
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import render

from posts.models import Author
from contact_messages.models import ContactMessage
from contact_messages.forms import ContactForm 

def index(request):
	return render(request, 'index.html')

def services(request):
	return render(request, 'services.html')

def team(request):
	team = Author.objects.staff()
	return render(request, 'team.html', {
		'team':team,
	})

def contact(request):
	authors = Author.objects.all()
	form = ContactForm()

	if request.method == 'POST':
		form = ContactForm(request.POST)
		if form.is_valid():
			obj = ContactMessage()
			obj.name = form.cleaned_data['name']
			obj.email = form.cleaned_data['email']
			obj.website = form.cleaned_data['website']
			obj.subject = form.cleaned_data['subject']
			obj.message = form.cleaned_data['message']
			obj.save()
			
			messages.add_message(request, messages.SUCCESS,
								'Thank you!')
			return HttpResponseRedirect(reverse('contact'))

	return render(request, 'contact.html', {
			'form': form,
			'authors':authors,
		})