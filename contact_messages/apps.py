from django.apps import AppConfig


class ContactMessagesConfig(AppConfig):
    name = 'contact_messages'
