from django import forms
from django.core import validators

def must_be_empty(value):
	if value:
		raise forms.ValidationError('is not empty.')

class ContactForm(forms.Form):
	name = forms.CharField(label='', 
						   widget=forms.TextInput(attrs={
						   		'class': 'form-control',
						   		'placeholder': 'Name (Required)',
						   	}))
	email = forms.EmailField(label='',
							 widget=forms.TextInput(attrs={
							 	'class': 'form-control',
							 	'placeholder': 'Email (Required)',
							 	}))
	website = forms.URLField(required=False, label='',
							 widget=forms.TextInput(attrs={
							 	'class': 'form-control',
							 	'placeholder': 'Website',
							 	}))
	subject = forms.CharField(label='', 
							  widget=forms.TextInput(attrs={
							  	'class': 'form-control',
							  	'placeholder': 'Subject (Required)',
							 	}))
	message = forms.CharField(widget=forms.Textarea(attrs={
								'class': 'form-control',
								'placeholder': 'Message (Required)',
								}), label='')
	bees = forms.CharField(required=False, 
						   widget=forms.HiddenInput, 
						   label="Leave Empty",
						   validators=[must_be_empty]
						   )