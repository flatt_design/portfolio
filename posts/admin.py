from django.contrib import admin

from .models import Post, Category, PostImage, Author

class ImageInline(admin.StackedInline):
	model = PostImage

class PostAdmin(admin.ModelAdmin):
	list_display = ["__str__", "post_type", "author", "published"]

	def author(self, obj):
		return obj.author.display_name

	def published(self, obj):
		if obj.is_published:
			return True
		else:
			return False

	inlines = [ImageInline,]

class AuthorAdmin(admin.ModelAdmin):
	list_display = ["__str__", "title"]

admin.site.register(Category)
admin.site.register(Author, AuthorAdmin)
admin.site.register(Post, PostAdmin)