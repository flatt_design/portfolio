from django.shortcuts import get_object_or_404, render

from .models import Post, Category, POST_TYPES

def posts_index(request):
	"""
	All blog posts
	"""
	posts = Post.objects.published()
	tags = Category.objects.all()
	types = POST_TYPES

	return render(request, 'posts/posts_index.html', {
		'posts': posts,
		'tags': tags,
		'types': types,
	})

def posts_category(request, tag):
	"""

	"""
	posts = Post.objects.published()
	tags = Category.objects.exclude(slug=tag)

	return render(request, 'posts/posts_index.html', {
		'posts': posts,
		'tags': tags
	})

def post_detail(request, pk):
	"""
	Single post detail
	"""
	post = get_object_or_404(Post, pk=pk)
	return render(request, 'posts/post_detail.html', {
		'post': post,
	})