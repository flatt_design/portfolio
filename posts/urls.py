from django.conf.urls import url

from . import views

urlpatterns = [
	url(r'^$', views.posts_index, name='index'),
	url(r'(?P<pk>\d+)/$', views.post_detail, name='detail'),
	url(r'(?P<tag>[\w-]+)/$', views.posts_category, name='category')
]