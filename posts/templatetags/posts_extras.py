import markdown
from django import template 
from flatt_design.settings import MARKDOWNX_MARKDOWN_EXTENSIONS, MARKDOWNX_MARKDOWN_EXTENSION_CONFIGS

register = template.Library()

@register.filter(name='markdownify')
def markdownify(content):
	return markdown.markdown(content, extensions=MARKDOWNX_MARKDOWN_EXTENSIONS,
							extension_configs=MARKDOWNX_MARKDOWN_EXTENSION_CONFIGS)

@register.filter(name='inlist')
def inlist(value, list_):
	return value in list_.split(',')
