# -*- coding: utf-8 -*-
# Generated by Django 1.10.2 on 2016-11-06 16:45
from __future__ import unicode_literals

from django.db import migrations
import tinymce.models


class Migration(migrations.Migration):

    dependencies = [
        ('posts', '0004_auto_20161105_1511'),
    ]

    operations = [
        migrations.AlterField(
            model_name='post',
            name='body',
            field=tinymce.models.HTMLField(blank=True),
        ),
    ]
