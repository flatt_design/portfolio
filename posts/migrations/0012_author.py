# -*- coding: utf-8 -*-
# Generated by Django 1.10.2 on 2016-11-11 22:21
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('posts', '0011_auto_20161109_1108'),
    ]

    operations = [
        migrations.CreateModel(
            name='Author',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('display_name', models.CharField(max_length=255, unique=True)),
                ('slug', models.SlugField(unique=True)),
                ('bio', models.CharField(max_length=255, unique=True)),
                ('website_url', models.URLField(blank=True, default='', max_length=2000, null=True)),
                ('facebook_url', models.URLField(blank=True, default='', max_length=2000, null=True)),
                ('twitter_url', models.URLField(blank=True, default='', max_length=2000, null=True)),
                ('linkedin_url', models.URLField(blank=True, default='', max_length=2000, null=True)),
                ('image', models.ImageField(help_text='Minimum Size: 200x200', upload_to='posts/authors')),
            ],
            options={
                'verbose_name': 'Blog Author',
                'verbose_name_plural': 'Blog Authors',
            },
        ),
    ]
